# Test for local cosmosDB
from azure.cosmos import CosmosClient, PartitionKey, documents
from azure.cosmos.exceptions import CosmosResourceNotFoundError
import asyncio
from uuid import uuid4
import time
import random
import os
import timeit

def current_milli_time():
    return timeit.default_timer() * 1000

def connect_to_database(credentials_file, preferred_regions_file=None):
    # Read credentials from file. Endpoint on first line, primary key on second
    with open(credentials_file, 'r') as f:
        endpoint = f.readline().strip()
        primary_key = f.readline().strip()
        database_name = f.readline().strip()
        container_name = f.readline().strip()
    if preferred_regions_file:
        # Get preferred regions from file "preferred_regions.txt", as it's meas.point-specific:
        preferred_regions_list = []
        with open(preferred_regions_file, 'r') as f:
            for line in f:
                preferred_regions_list.append(line.strip())
        # Set up local cosmosDB
        azure_connection_policy = documents.ConnectionPolicy()
        azure_connection_policy.PreferredLocations = preferred_regions_list
        client = CosmosClient(endpoint, primary_key, connection_policy=azure_connection_policy)
    else:
        client = CosmosClient(endpoint, primary_key)
    database = client.create_database_if_not_exists(id=database_name)
    return database.get_container_client(container_name)

def main(argv):
    # Get number of items to be selected from args
    num_items = int(argv[1])
    # Parse each file from targets directory
    for filename in os.listdir('targets'):
        # Check if preferred regions are defined. If no, use default connection policy
        if os.path.isfile(filename + '_preferred_regions.txt'):
            container = connect_to_database('targets/' + filename, filename + '_preferred_regions.txt')
        else:
            container = connect_to_database('targets/' + filename)

        # Get oldest modified
        select_q = "SELECT * FROM c ORDER BY c._ts ASC OFFSET 0 LIMIT {}".format(num_items)

        # Test

        # * SELECT
        start_time = current_milli_time()
        items = list(container.query_items(query=select_q, enable_cross_partition_query=True))
        select_time = current_milli_time() - start_time
        request_charge = container.client_connection.last_response_headers['x-ms-request-charge']

        # * DELETE
        firstname = items[0]['firstname']
        delete_q = "DELETE FROM c WHERE c.firstname = '{}'".format(firstname)
        start_time = current_milli_time()
        container.query_items(query=delete_q, enable_cross_partition_query=True)
        delete_time = current_milli_time() - start_time

        # * INSERT
        # Re-create the deleted item with original score + 5
        score = items[0]['score'] + 5
        if score > 100:
            score = 0
        insert_q = "INSERT INTO c (id, firstname, score) VALUES ('{}', '{}', {})".format(str(uuid4()), firstname, score)
        start_time = current_milli_time()
        container.query_items(query=insert_q, enable_cross_partition_query=True)
        insert_time = current_milli_time() - start_time

        # Report
        # Check if 'results' folder exists
        if not os.path.exists('results'):
            os.mkdir('results')
        # Write results to file
        with open('results/' + filename, 'a') as f:
            f.write("{},{},{}\n".format(select_time, delete_time, insert_time))
    
if __name__ == "__main__":
    main()