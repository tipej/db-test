# Test for local cosmosDB
from azure.cosmos import CosmosClient, PartitionKey
from azure.cosmos.exceptions import CosmosResourceNotFoundError
import asyncio
from uuid import uuid4
import time
import random
import sys
import os

# def clean_database(client, db_name, container_name):
#     # Get the container
#     database = client.create_database_if_not_exists(id=db_name)
#     container = database.get_container_client(container_name)
#     try:
#         database.delete_container(container_name)
#     except CosmosResourceNotFoundError:
#         pass
#     return

def populate_database(container, items):
    # Create items
    for item in items:
        container.upsert_item(item)

def connect_to_database(credentials_file):
    # Read credentials from file. Endpoint on first line, primary key on second
    with open(credentials_file, 'r') as f:
        endpoint = f.readline().strip()
        primary_key = f.readline().strip()
        database_name = f.readline().strip()
        container_name = f.readline().strip()
    # Set up local cosmosDB
    client = CosmosClient(endpoint, primary_key)
    database = client.create_database_if_not_exists(id=database_name)
    return database.get_container_client(container_name)

def main(argv):
    # Get number of items from args
    num_items = int(argv[1])
    items = []
    for i in range(0, num_items):
        item = {
            'firstname': 'John' + str(i),
            'id': str(uuid4()),
            'score': random.randint(0, 100),
        }
        items.append(item)
    # Find every .txt file in the targets directory
    for filename in os.listdir('targets'):
        container = connect_to_database('targets/' + filename)
        populate_database(container, items)
        print('Populated database {}'.format(filename))
    
if __name__ == "__main__":
    main(sys.argv)